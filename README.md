Used plugins
------------

- pathogen
- nerd-commenter (Alt-C Toggle)
- snipMate [pathogen]
- closepairs
- surround
- toggle (Alt-t)
- zencoding (emmet) [pathogen]


Install (RECOMENDED)
=======

	git clone https://bitbucket.org/gordio/vimrc ~/.vim
	ln -s ~/.vim/vimrc ~/.vimrc
	ln -s ~/.vim/gvimrc ~/.gvimrc
	git submodule update --init


Install (without git)
=======

	URL="https://bitbucket.org/gordio/vimrc/get/master.tar.gz"
	(curl $URL 2>/dev/null || wget $URL -O - ) | tar zxf -
	mv gordio-vimrc-* ~/.vim
	ln -s .vim/vimrc .vimrc
	ln -s .vim/gvimrc .gvimrc