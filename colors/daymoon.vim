" Vim color scheme
"
" Name:         daymoon.vim
" Maintainer:   Gordio <gordi.oleg@gmail.com>
" Last Change:  01 Mar 2011
" License:      MIT
" Version:      1.0
"
" To use for gvim:
" 1: install this file as ~/.vim/colors/railscasts.vim
" 2: put "colorscheme daymoon" in your .gvimrc

set background=light
hi clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "daymoon"

hi link htmlTag                     xmlTag
hi link htmlTagName                 xmlTagName
hi link htmlEndTag                  xmlEndTag

hi Normal           guifg=#303030   guibg=#F2F1F0
hi Cursor           guifg=NONE      guibg=#A09050
""hi CursorLine       guifg=#FF0000   guibg=#FF0000
hi SpecialKey       guifg=#BDBCBA   guibg=NONE
hi LineNr           guifg=#666666   guibg=#F2F1F0
hi TabLineFill      guifg=#F2F1F0   guibg=#F2F1F0
hi TabLine          guifg=#888888   guibg=#F2F1F0   gui=NONE
hi TabLineSel       guifg=#555555   guibg=#E2E1F0   gui=NONE
hi StatusLine       guifg=#333333   guibg=#B2AFA7   gui=NONE
hi StatusLineNC     guifg=#444444   guibg=#B2AFA7   gui=NONE

hi IncSearch        guifg=#FFFFFF   guibg=#FF8000   gui=NONE
hi Search           guifg=#303030   guibg=#F0D090   gui=NONE
hi SpellBad         guifg=NONE      guibg=NONE      gui=undercurl   guisp=#F01818
hi SpellCap         guifg=NONE      guibg=NONE      gui=undercurl   guisp=#14B9C8
hi SpellRare        guifg=NONE      guibg=NONE      gui=undercurl   guisp=#4CBE13
hi SpellLocal       guifg=NONE      guibg=NONE      gui=undercurl   guisp=#000000

hi ErrorMsg         guifg=#FFFFFF   guibg=#A40000

hi DiffAdd                          guibg=#DEFFCD
hi DiffChange                       guibg=#DAD7FF
hi DiffDelete       guifg=#C8C8C8   guibg=#FFFFFF   gui=none
hi DiffText         guifg=#FFFFFF   guibg=#767396   gui=none

hi pythonBuiltin    guifg=#303030                   gui=BOLD

hi Number           guifg=#6D1000
hi Keyword          guifg=#B26A36                   gui=NONE
hi Comment          guifg=#9C8458
hi PreProc          guifg=#004080
hi Function         guifg=#804309                   gui=NONE
hi Identifier       guifg=#4E9A3F
hi String           guifg=#305700
hi Statement        guifg=#602000                   gui=NONE " func in python
hi Type             guifg=#A62E19                   gui=NONE
hi Constant         guifg=#3D1547
hi Special          guifg=#941010
hi Ignore           guifg=#B0B0B0
hi Error            guifg=#FFFFFF   guibg=#A00000
hi Todo             guifg=#A00000   guibg=#F1EFBA   gui=BOLD
"hi Define                    guifg=#CC7833    ctermfg=173
"hi Error                     guifg=#FFC66D    ctermfg=221 guibg=#990000 ctermbg=88
"hi Include                   guifg=#CC7833    ctermfg=173 gui=NONE cterm=NONE
"hi PreCondit                 guifg=#CC7833    ctermfg=173 gui=NONE cterm=NONE
"hi Title                     guifg=#FFFFFF    ctermfg=15
"hi Visual                    guibg=#5A647E    ctermbg=60
"
"hi DiffAdd                   guifg=#E6E1DC    ctermfg=7 guibg=#519F50 ctermbg=71
"hi DiffDelete                guifg=#E6E1DC    ctermfg=7 guibg=#660000 ctermbg=52
"hi Special                   guifg=#DA4939    ctermfg=167
"
"hi rubyBlockParameter        guifg=#FFFFFF    ctermfg=15
"hi rubyClass                 guifg=#FFFFFF    ctermfg=15
"hi rubyConstant              guifg=#DA4939    ctermfg=167
"hi rubyInstanceVariable      guifg=#D0D0FF    ctermfg=189
"hi rubyInterpolation         guifg=#519F50    ctermfg=107
"hi rubyLocalVariableOrMethod guifg=#D0D0FF    ctermfg=189
"hi rubyPredefinedConstant    guifg=#DA4939    ctermfg=167
"hi rubyPseudoVariable        guifg=#FFC66D    ctermfg=221
"hi rubyStringDelimiter       guifg=#A5C261    ctermfg=143
"
"hi xmlTag                    guifg=#E8BF6A    ctermfg=179
"hi xmlTagName                guifg=#E8BF6A    ctermfg=179
"hi xmlEndTag                 guifg=#E8BF6A    ctermfg=179

"hi mailSubject               guifg=#A5C261    ctermfg=107
"hi mailHeaderKey             guifg=#FFC66D    ctermfg=221
"hi mailEmail                 guifg=#A5C261    ctermfg=107 gui=italic cterm=underline
"
"hi SpellBad                  guifg=#D70000    ctermfg=160 ctermbg=NONE cterm=underline
"hi SpellRare                 guifg=#D75F87    ctermfg=168 guibg=NONE ctermbg=NONE gui=underline cterm=underline
"hi SpellCap                  guifg=#D0D0FF    ctermfg=189 guibg=NONE ctermbg=NONE gui=underline cterm=underline
"hi MatchParen                guifg=#FFFFFF    ctermfg=15 guibg=#005f5f ctermbg=23


"hi link String Constant
"hi link Character Constant
"hi link Number Constant
"hi link Boolean Constant
"hi link Float Number
"hi link Conditional Repeat
"hi link Label Statement
"hi link Keyword Statement
"hi link Exception Statement
"hi link Include PreProc
"hi link Define PreProc
"hi link Macro PreProc
"hi link PreCondit PreProc
"hi link StorageClass Type
"hi link Structure Type
"hi link Typedef Type
"hi link Tag Special
"hi link SpecialChar Special
"hi link Delimiter Special
"hi link SpecialComment Special
"hi link Debug Special
