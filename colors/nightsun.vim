" Name: Night Sun
" Author: Gordio [admin@gordio.pp.ua]
" Licence: MIT, GPL

set background=dark
if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif
let g:colors_name="nightsun"


hi link htmlTag      xmlTag
hi link htmlTagName  xmlTagName
hi link htmlEndTag   xmlEndTag

hi Cursor                    guifg=#000000    ctermfg=0 guibg=#FFFFFF ctermbg=15
hi CursorLine                guibg=#000000    ctermbg=233 cterm=NONE

" Tab line
hi TabLineFill  guifg=#181818   guibg=NONE    gui=NONE
hi TabLine      guifg=#202020   guibg=#AAAAAA gui=NONE
hi TabLineSel   guifg=#000000   guibg=#E0E0E0 gui=NONE

hi MatchParen                guifg=#EEEEEE guibg=#131210   gui=NONE

hi Constant                  guifg=#6D9CBE    ctermfg=73
hi Define                    guifg=#6D9CBE    ctermfg=173
hi DefinedName               guifg=#6D9CBE    ctermfg=173
hi Error                     guifg=#FFC66D    ctermfg=221 guibg=#990000 ctermbg=88
hi Function                  guifg=#FFC66D    ctermfg=221 gui=NONE cterm=NONE
hi Identifier                guifg=#6D9CBE    ctermfg=73 gui=NONE cterm=NONE
hi Include                   guifg=#DC8843    ctermfg=173 gui=NONE cterm=NONE
hi PreCondit                 guifg=#DC8843    ctermfg=173 gui=NONE cterm=NONE
hi Keyword                   guifg=#DC8843    ctermfg=173 cterm=NONE
hi Number                    guifg=#A8C661    ctermfg=107
hi PreProc                   guifg=#D6D1CC    ctermfg=103
hi Search                    guifg=NONE       ctermfg=NONE guibg=#2a2a2a ctermbg=235
hi Statement                 guifg=#DC8843    ctermfg=173 gui=NONE cterm=NONE
hi String                    guifg=#A5C261    ctermfg=107
hi Title                     guifg=#FFFFFF    ctermfg=15
hi Type                      guifg=#DA4939    ctermfg=167 gui=NONE cterm=NONE
hi Visual                    guibg=#5A647E    ctermbg=60

hi DiffAdd                   guifg=#D6D1CC    ctermfg=7 guibg=#519F50 ctermbg=71
hi DiffDelete                guifg=#D6D1CC    ctermfg=7 guibg=#660000 ctermbg=52
hi Special                   guifg=#DA4939    ctermfg=167
hi Folded                    guibg=NONE       ctermfg=4        ctermbg=NONE    guifg=#3465a4
hi FoldColumn                guibg=#000000    ctermfg=4        ctermbg=0        guifg=#3465a4

hi Member                          guifg=#DC8843
hi GlobalVariable                  guifg=#DC8843

hi Todo                            guifg=#DCB478 guibg=#202020 gui=bold

hi pythonBuiltin             guifg=#6D9CBE    ctermfg=73 gui=NONE cterm=NONE
hi rubyBlockParameter        guifg=#FFFFFF    ctermfg=15
hi rubyClass                 guifg=#FFFFFF    ctermfg=15
hi rubyConstant              guifg=#DA4939    ctermfg=167
hi rubyInstanceVariable      guifg=#D0D0FF    ctermfg=189
hi rubyInterpolation         guifg=#519F50    ctermfg=107
hi rubyLocalVariableOrMethod guifg=#D0D0FF    ctermfg=189
hi rubyPredefinedConstant    guifg=#DA4939    ctermfg=167
hi rubyPseudoVariable        guifg=#FFC66D    ctermfg=221
hi rubyStringDelimiter       guifg=#A5C261    ctermfg=143

hi xmlTag                    guifg=#E8BF6A    ctermfg=179
hi xmlTagName                guifg=#E8BF6A    ctermfg=179
hi xmlEndTag                 guifg=#E8BF6A    ctermfg=179

hi mailSubject               guifg=#A5C261    ctermfg=107
hi mailHeaderKey             guifg=#FFC66D    ctermfg=221
hi mailEmail                 guifg=#A5C261    ctermfg=107 gui=italic cterm=underline

hi SpellBad                  guifg=#D70000    ctermfg=160 ctermbg=NONE cterm=underline
hi SpellRare                 guifg=#D75F87    ctermfg=168 guibg=NONE ctermbg=NONE gui=underline cterm=underline
hi SpellCap                  guifg=#D0D0FF    ctermfg=189 guibg=NONE ctermbg=NONE gui=underline cterm=underline

hi Normal       guifg=#E5E0DD   guibg=#222120 gui=NONE
hi ColorColumn                  guibg=#282220 gui=NONE
hi OverLength                   guibg=#3A0A0A gui=NONE
hi FoldColumn   guifg=#929F9F   guibg=#151515 gui=NONE
hi Folded       guifg=#928F9F   guibg=#1A1A1A gui=NONE
hi LineNr       guifg=#808080   guibg=#20201F gui=NONE ctermfg=159
hi SpecialKey   guifg=#707275   guibg=NONE    gui=NONE
hi NonText      guifg=#404245                 gui=NONE
hi StatusLine   guifg=#C0C0C0   guibg=#151515 gui=NONE
hi StatusLineNC guifg=#808080   guibg=#1A1A1A gui=NONE
hi WarningMsg   guifg=#A40000   guibg=#101010 gui=NONE
hi Error        guifg=#A40000   guibg=#101010 gui=NONE
hi ErrorMsg     guifg=#A40000   guibg=#101010 gui=NONE
hi Comment      guifg=#9C8458                 gui=NONE ctermfg=180
hi PreProc      guifg=#6D9CBE                 gui=NONE
hi SignColumn                   guibg=#20201F gui=NONE

" Spell scheme
hi SpellBad                                   gui=UNDERLINE
hi SpellCap                                   gui=UNDERLINE
hi SpellRare                                  gui=UNDERLINE
hi SpellLocal                                 gui=UNDERLINE

" Menu scheme
hi Pmenu        guifg=#2A2A2A   guibg=#CDA869 gui=NONE
hi PmenuSel     guifg=#F8F8F8   guibg=#9B703F gui=NONE
hi PmenuSbar    guibg=#DAEFA3                 gui=NONE
hi PmenuThumb   guifg=#8F9D6A                 gui=NONE

" Statusbar requirements
hi sl_branch    guifg=#FFC66D   guibg=#151515 gui=NONE
hi sl_file      guifg=#F0F0F0   guibg=#151515 gui=NONE
hi sl_minor     guifg=#777777   guibg=#151515 gui=NONE

" 16 color terminals
if &t_Co == 16
	hi Todo       ctermfg=1
	hi LineNr     ctermfg=8 ctermbg=NONE
	hi OverLength ctermfg=1 ctermbg=1
	hi NonText    ctermfg=8

	hi Comment    ctermfg=6 ctermbg=NONE
	hi PreProc    ctermfg=12
	hi SpecialKey ctermfg=8
	hi Function   ctermfg=11
	hi Keyword    ctermfg=4
	hi Constant   ctermfg=13
	hi String     ctermfg=10
	hi Type       ctermfg=9
	hi Statement  ctermfg=14
	hi PreCondit  ctermfg=1
	hi Identifier ctermfg=12
	hi Include    ctermfg=5
	hi Define     ctermfg=5

	" Status line
	hi StatusLine ctermfg=0 ctermbg=8
	hi StatusLineNC ctermfg=0 ctermbg=8
	hi sl_branch ctermfg=6
	hi sl_file   ctermfg=7
	hi sl_minor  ctermfg=8
	hi sl_error  ctermfg=1

	hi Todo      ctermfg=14 ctermbg=NONE

	" syntax
	hi link htmlArg Statement
endif
