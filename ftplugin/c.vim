setl cindent textwidth=80
map <F6>   :!clear;flawfinder -SQDI -m 0 % 2>&1 \| more<CR>
map <M-F6> :!clear;splint -preproc % 2>&1 \| more<CR>
map <F9>   :w:make<CR>
