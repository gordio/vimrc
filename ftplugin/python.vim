setl ai ts=4 sw=4 sts=4 fo=croql omnifunc=pythoncomplete#Complete
map <F6>   :!clear;pyflakes % 2>&1 \| more<CR>
map <M-F6> :!clear;pep8 % 2>&1 \| more<CR>
map <F9>   :w:!python % 2>&1 \| more<CR>
