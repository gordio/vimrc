set columns=86 lines=40
if has('win32') || has('win64')
	"set guifont=Lucida_Console:h12:cRUSSIAN::
	set guifont=Terminus:h12:cRUSSIAN::
	set linespace=-1
elseif has('mac')
	"set guifont=Terminus\ Medium:h16
	set guifont=Envy\ Code\ R:h13
else
	set guifont=Ubuntu\ Mono\ 13
	"set guifont=Liberation\ Mono\ 12
	"set guifont=Terminus\ 14
	set linespace=0
endif

set guioptions=c
if !has('mac')
	nmap <silent><M-M> :set guioptions+=m<CR>
	imap <silent><M-M> <C-O>:set guioptions+=m<CR>
	vmap <silent><M-M> <ESC>:set guioptions+=m<CR>
	nmap <silent><M-m> :set guioptions-=m<CR>
	imap <silent><M-m> <C-O>:set guioptions-=m<CR>
	vmap <silent><M-m> <ESC>:set guioptions-=m<CR>
endif

colorscheme nightsun
