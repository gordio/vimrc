" Author: Timo Teifel (rewrited by Gordio)
" Version: 0.3.2
" Date: 06 Feb 2013
" Licence: GPL v2.0
"
"  true     <->     false
"  on       <->     off
"  yes      <->     no
"  +        <->     -
"  1        <->     0
"  >        <->     <
"  define   <->     undef
"  accept   <->     drop
"  ||       <->     &&
"  |        <->     &
"
" Thanks:
" - Christoph Behle, who inspired me to write this
" - Jan Christoph Ebersbach, for the 'keep case' patch
" - the Vim Documentation ;)
"
" let loaded_toggle=1

let s:save_cpo = &cpo
set cpo&vim

" some Helper functions {{{
function! s:Toggle_changeChar(string, pos, char)
  return strpart(a:string, 0, a:pos) . a:char . strpart(a:string, a:pos+1)
endfunction

function! s:Toggle_insertChar(string, pos, char)
  return strpart(a:string, 0, a:pos) . a:char . strpart(a:string, a:pos)
endfunction

function! s:Toggle_changeString(string, beginPos, endPos, newString)
  return strpart(a:string, 0, a:beginPos) . a:newString . strpart(a:string, a:endPos+1)
endfunction
" }}}

function! Toggle() "{{{
	" save values which we have to change temporarily:
	let s:lineNo = line(".")
	let s:columnNo = col(".")

	" Gather information needed later
	let s:cline = getline(".")
	let s:charUnderCursor = strpart(s:cline, s:columnNo-1, 1)

	let s:toggleDone = 0
	" 1. Check if the single Character has to be toggled {{{
	if (s:charUnderCursor == "+")
		execute "normal r-"
		let s:toggleDone = 1
	elseif (s:charUnderCursor == "-")
		execute "normal r+"
		let s:toggleDone = 1
	elseif (s:charUnderCursor == "<")
		execute "normal r>"
		let s:toggleDone = 1
	elseif (s:charUnderCursor == ">")
		execute "normal r<"
		let s:toggleDone = 1
	elseif (s:charUnderCursor == "1")
		execute "normal r0"
		let s:toggleDone = 1
	elseif (s:charUnderCursor == "0")
		execute "normal r1"
		let s:toggleDone = 1
	endif " }}}

	" 2. Check if cursor is on one-or two-character symbol"{{{
	if s:toggleDone == 0
	  let s:nextChar = strpart(s:cline, s:columnNo, 1)
	  let s:prevChar = strpart(s:cline, s:columnNo-2, 1)
	  if s:charUnderCursor == "|"
		if s:prevChar == "|"
		  execute "normal r&hr&"
		  let s:toggleDone = 1
		elseif s:nextChar == "|"
		  execute "normal r&lr&"
		  let s:toggleDone = 1
		else
		  execute "normal r&"
		  let s:toggleDone = 1
		end
	  end

	  if s:charUnderCursor == "&"
		if s:prevChar == "&"
		  execute "normal r|hr|"
		  let s:toggleDone = 1
		elseif s:nextChar == "&"
		  execute "normal r|lr|"
		  let s:toggleDone = 1
		else
		  execute "normal r|"
		  let s:toggleDone = 1
		end
	  end
	endif"}}}

	" 3. Check if complete word can be toggled {{{
	if (s:toggleDone == 0)
		let s:wordUnderCursor_tmp = ''
"
		let s:wordUnderCursor = expand("<cword>")
		if (s:wordUnderCursor ==? "true")
			let s:wordUnderCursor_tmp = "false"
			let s:toggleDone = 1
		elseif (s:wordUnderCursor ==? "false")
			let s:wordUnderCursor_tmp = "true"
			let s:toggleDone = 1

		elseif (s:wordUnderCursor ==? "on")
			let s:wordUnderCursor_tmp = "off"
			let s:toggleDone = 1
		elseif (s:wordUnderCursor ==? "off")
			let s:wordUnderCursor_tmp = "on"
			let s:toggleDone = 1

		elseif (s:wordUnderCursor ==? "accept")
			let s:wordUnderCursor_tmp = "drop"
			let s:toggleDone = 1
		elseif (s:wordUnderCursor ==? "drop")
			let s:wordUnderCursor_tmp = "accept"
			let s:toggleDone = 1

		elseif (s:wordUnderCursor ==? "yes")
			let s:wordUnderCursor_tmp = "no"
			let s:toggleDone = 1
		elseif (s:wordUnderCursor ==? "no")
			let s:wordUnderCursor_tmp = "yes"
			let s:toggleDone = 1
		elseif (s:wordUnderCursor ==? "define")
			let s:wordUnderCursor_tmp = "undef"
			let s:toggleDone = 1
		elseif (s:wordUnderCursor ==? "undef")
			let s:wordUnderCursor_tmp = "define"
			let s:toggleDone = 1
		endif

		 " preserve case (provided by Jan Christoph Ebersbach)
		 if s:toggleDone
			 if strpart (s:wordUnderCursor, 0) =~ '^\u*$'
				 let s:wordUnderCursor = toupper (s:wordUnderCursor_tmp)
			 elseif strpart (s:wordUnderCursor, 0, 1) =~ '^\u$'
				 let s:wordUnderCursor = toupper (strpart (s:wordUnderCursor_tmp, 0, 1)).strpart (s:wordUnderCursor_tmp, 1)
			 else
				 let s:wordUnderCursor = s:wordUnderCursor_tmp
			 endif
		 endif

		" if wordUnderCursor is changed, set the new line
		if (s:toggleDone == 1)
			execute "normal ciw" . s:wordUnderCursor
			let s:toggleDone = 1
		endif

	endif " toggleDone?}}}

	if s:toggleDone == 0
	  echo "Can't toggle word under cursor, word is not in list."
	endif

	" unlet used variables to save memory {{{
	unlet! s:charUnderCursor
	unlet! s:toggleDone
	unlet! s:cline
	unlet! s:foundSpace
	unlet! s:cuc "}}}

	"restore saved values
	call cursor(s:lineNo,s:columnNo)
	unlet s:lineNo
	unlet s:columnNo
endfunction " }}}

let &cpo = s:save_cpo
unlet s:save_cpo

" vim:fdm=marker commentstring="%s
