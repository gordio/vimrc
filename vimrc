set nocompatible
scriptencoding utf-8
lang mes C
"set verbose=1
call pathogen#infect()
" disable plugins
let g:loaded_matchparen = 1
let g:loaded_getscriptPlugin = "v34"

" Esc timeout on Mac
set timeoutlen=0 ttimeoutlen=0

if &term =~ "xterm" || &term =~ "rxvt"
	set t_Co=256
	colorscheme nightsun
elseif &term =~ "linux" && &term =~ "screen"
	set t_Co=16
	colorscheme nightsun
else
	set t_Co=16
	colorscheme nightsun
endif

syntax on
filetype on
filetype indent on
filetype plugin on

" Просмотр нетекстовых файлов в Vim
au! BufReadPost *.doc silent %!antiword "%"
au! BufReadPost *.odt silent %!odt2txt "%"

if &fileencodings !~? "utf-8"
	set termencoding=utf-8
	set encoding=utf-8
	" Порядок перебора файлов
	set ffs=unix,dos,mac
	" Порядок перебора кодировок
	set fileencodings=utf-8,windows-1251,cp866,koi8-r,iso-8859-15
endif

set noexrc " don't use local version of .(g)vimrc, .exrc

set wildmenu
set wildcharm=<TAB>
set wildmode=list:longest,full
set wildignore=*~,*.pyc,*.pyo,*.o,*.so,*.ko,*.obj,*.dll,*.bak,*.exe,*.jpg,*.gif,*.png

if has('clipboard')
	set pastetoggle=<C-F12>  " Переключение режима отступов при вставке
	set mouse=nvir           " разрешить визуальное выделение мышью
	set clipboard=autoselect " использовать буфер мыши при выделении
endif

set cpoptions=aABceFsq
"             ||||||||
"             |||||||+-- When joining lines, leave the cursor between joined lines
"             ||||||+-- Set buffer options when entering the buffer
"             |||||+-- :write command updates current file name
"             ||||+-- Automatically add <CR> to the last line when using :@r
"             |||+-- Searching continues at the end of the match at the cursor position
"             ||+-- A backslash has no special meaning in mappings
"             |+-- :write updates alternative file name
"             +-- :read updates alternative file name
set shortmess=aOstT " shortens messages to avoid

set modeline                             " Читать параметры конфигурации из открытого файла
set title                                " Отображать имя активного буфера в заголовке
set backup                               " Включить сохранение резервных копий
set sessionoptions=buffers,tabpages,help " настройки файлов с сессиями
if has('win32') || has('win64')
	set viminfo='128,/128,:128,<128,s10,h,n~/vimfiles/sessions/viminfo
	set directory=~/vimfiles/sessions/       " Здесь храним сесии
else
	set directory=~/.vim/sessions/       " Здесь храним сесии
	set viminfo='128,/128,:128,<128,s10,h,n~/.vim/sessions/viminfo
endif
if v:version >= 703
	"undo settings
	if has('win32') || has('win64')
		set undodir=~/vimfiles/undofiles
	else
		set undodir=~/.vim/undofiles
	endif
	set undofile

	set colorcolumn=+1 "mark the ideal max text width
endif
"set hidden        " Не выгружать скрытые буферы
set history=32     " Запоминать столько истории
set undolevels=32  " Запоминать столько уровней отмены
set makeprg=make\ -j3
set autoread       " Перечитывать изменённые файлы автоматически

set tabpagemax=12   " Максимальное число вкладок
set nostartofline   " Не менять позицию курсора при прыжках по буферу
set number          " Отображать колонку нумерации строк
set numberwidth=5   " Размер колонки нумерации строк
set lazyredraw      " Не перерисовывать буфер на макросах
set autochdir       " Менять текущий каталог на каталог активного буфера

set showcmd         " Use history on non completed command
set cmdheight=1     " Сделать строку команд больше?
set cmdwinheight=10 " Сделать окно команд больше
set showtabline=1   " Показывать строку вкладок всегда

set incsearch       " Использовать инкрементальный поиск
set hlsearch        " Использовать подсветку поиска
set ignorecase      " Игнорировать регистр при поиске ...
set smartcase       " ... если поисковый запрос в нижнем регистре
set infercase       " Предлагать авто-дополнение на основе уже введённог

" Code formating
set autoindent                 " Наследовать отступы предыдущей строки
set smartindent                " Умные отступы на основе синтаксиса
set formatoptions=crq          " :h fo-table
set cindent
set cinoptions=:1s,l1,t0,g0,j1 " :h cinoptions-values
set tw=80                      " Длина строки (для переносов)
set nowrap                     " визуально переносить строки
set backspace=indent,eol,start " make backspace a more flexible
"set backspace=2               " Разрешить backspace в режиме вставки
set noexpandtab                " неЗаменять <Tab> пробелами
set smarttab
set tabstop=4                  " отображать табуляцию в [n] пробела ...
set softtabstop=4              " что бы при бэкспейсе понимать спейсные табы
set shiftwidth=4               " ... и сдвигов строк
set noshiftround               " Удалять лишние пробелы при отступе
"set noshowmatch                  " Подсвечивать парные скобку
set matchtime=0                " -|-|- с задержкой

"set matchpairs+=<:>           " Показывать совпадающие скобки для HTML-тегов

set laststatus=2               " Показывать строку статуса всегда

set mouse=a                    " Use mouse
set ttymouse=xterm2

set confirm               " Использовать диалоги вместо сообщений об ошибках
set shortmess=fimnrxoOtTI " использовать сокращённые диалоги
set report=0              " Показывать все изменения буфера

set scrolloff=2           " Расстояние до края при вертикальной прокрутке
set scrolljump=5          " Размер прыжка при вертикальной прокрутке
set sidescrolloff=2       " Расстояния до края при горизонтальной прокрутке
set sidescroll=5          " Размер прыжка при горизонтальной прокрутке
set splitbelow            " Разбивать окно горизонтально снизу
set splitright            " Разбивать окно вертикально справа

set noequalalways         " Не выравнивать размеры окон при закрытии
set nojoinspaces          " Не вставлять лишних пробелов при объединении строк

set ttyfast               " Fast key reaction (Used for pasts)
set foldenable            " Включаем скрутку
set foldmethod=marker     " Метод поиска блоков
 "syn region foldBraces start=/{/ end=/}/ transparent fold keepend extend
 "syn match foldImports /\(\n\?import.\+;\n\)\+/ transparent fold
"set foldtext=v:folddashes.substitute(getline(v:foldstart),'/\\*\\\|\\*/\\\|{{\\d\\=','','g')
set foldcolumn=0	" Use for fold bar << columns
set foldlevel=10	 " Hide fold blocks when hi big <<

" установим символы для подсветки
set list
"set listchars=tab:›\ ,trail:·,extends:»,precedes:«,nbsp:× ",eol:¬
set listchars=tab:>\ ,trail:×,extends:→,precedes:←,nbsp:• ",eol:¬


" STATUSLINE
set statusline=\ %<%#sl_file#%F%*                                    " Filename
set statusline+=%#sl_minor#%{&mod?'*':''}%h%r%*                      " File modes
set statusline+=%#sl_branch#%{StatuslineShowGitBranch()}%*           " git branch name
set statusline+=%#sl_minor#
set statusline+=\ >\ %*%{&ft!=''?&ft:'No-FT'}                        " filetype
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'\ \ >\ '.&fenc:''}    " file encoding
set statusline+=%{(&ff!='unix'&&&ff!='')?'\ \ >\ '.&ff:''}           " file endings
set statusline+=%*                                                   " use default color
set statusline+=\ %=                                                 " vim statusline left/right separator
"set statusline+={%{synIDattr(synID(line('.'),col('.'),1),'name')}}   " highlight
"set statusline+=\ \%w(%b,0x%B)                                       " char info
set statusline+=\ %#sl_minor#%c%*                                    " cursor position
set statusline+=\ (%l\ of\ %L)\ %P\                                     " offsets

fu! StatuslineShowGitBranch()
	if exists('*fugitive#statusline')
		return substitute(fugitive#statusline(), '\c\v\[?GIT\(([a-z0-9\-_\./:]+)\)\]?', ':\1', 'g')
	else
		return ''
	endif
endfu
" endSTATUSLINE


" SYNTAX
let g:perl_extended_vars = 1 " Highlight Perl var's inside strings

let g:python_highlight_all = 1

let g:go_highlight_array_whitespace_error = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_space_tab_error = 1
let g:go_highlight_trailing_whitespace_error = 1
let g:go_highlight_functions = 1

let g:html_use_css = 1
let g:html_no_rendering = 1
let g:use_xhtml = 0

let g:html5_event_handler_attributes_complete = 0
let g:html5_rdfa_attributes_complete = 0
let g:html5_microdata_attributes_complete = 0
let g:html5_aria_attributes_complete = 0
" endSYNTAX

" PLUGINS
let g:snips_author = "Gordio"

let g:tlTokenList = ["FIXME:", "TODO:", "XXX:"]


" Toggle plugin
nmap <silent> <M-t> :call Toggle()<CR>
imap <silent> <M-t> <C-O>:call Toggle()<CR>
vmap <silent> <M-t> <ESC>:call Toggle()<CR>
nmap <silent> <esc>t :call Toggle()<CR>
imap <silent> <esc>t <C-O>:call Toggle()<CR>
vmap <silent> <esc>t <ESC>:call Toggle()<CR>

" Toggle comment
imap <silent> <M-c> <C-O><plug>NERDCommenterToggle<CR>
nmap <silent> <M-c> <plug>NERDCommenterToggle
vmap <silent> <M-c> <plug>NERDComToggleComment
imap <silent> <esc>c <C-O><plug>NERDCommenterToggle<CR>
nmap <silent> <esc>c <plug>NERDCommenterToggle
vmap <silent> <esc>c <plug>NERDComToggleComment

" Toggle source/header
nmap <silent> <M-o> :A<CR>
imap <silent> <M-o> <C-O>:A<CR>
vmap <silent> <M-o> <ESC>:A<CR>
nmap <silent> <esc>o :A<CR>
imap <silent> <esc>o <C-O>:A<CR>
vmap <silent> <esc>o <ESC>:A<CR>
" endPLUGINS

" --- меню ---
if has('menu') && has("gui_running")
	" меню выбора типа конца строк
	anoremenu Tool.&EOL.&unix :setl fileformat=unix<CR>
	anoremenu Tool.&EOL.&dos  :setl fileformat=dos<CR>
	anoremenu Tool.&EOL.&mac  :setl fileformat=mac<CR>

	" меню выбора кодировки сохранения
	anoremenu Tool.E&ncoding.&Write.&utf-8    :setl fileencoding=utf-8<CR>
	anoremenu Tool.E&ncoding.&Write.&koi8-r   :setl fileencoding=koi8-r<CR>
	anoremenu Tool.E&ncoding.&Write.&win-1251 :setl fileencoding=windows-1251<CR>
	anoremenu Tool.E&ncoding.&Write.&dos-866  :setl fileencoding=cp866<CR>

	" меню выбора кодировки чтения
	anoremenu Tool.E&ncoding.&Read.&utf-8    :e ++enc=utf-8<CR>
	anoremenu Tool.E&ncoding.&Read.&koi8-r   :e ++enc=koi8-r<CR>
	anoremenu Tool.E&ncoding.&Read.&win-1251 :e ++enc=windows-1251<CR>
	anoremenu Tool.E&ncoding.&Read.&dos-866  :e ++enc=cp866<CR>

	" меню проверки орфографии
	anoremenu Tool.&Spell.&English  :setl spell spelllang=en<CR>
	anoremenu Tool.&Spell.&Russian  :setl spell spelllang=ru<CR>
	anoremenu Tool.&Spell.&Ukrain   :setl spell spelllang=uk<CR>
	anoremenu Tool.&Spell.&Combined :setl spell spelllang=en,ru<CR>
	anoremenu Tool.&Spell.&Off      :setl nospell spelllang=<CR>
endif


" Only do this part when compiled with support for autocommands
if has('autocmd')
	" автоматически перечитывать файл конфигурации VIM после его сохранения
	if !exists("*UpdateConfig")
		fu UpdateConfig()
			:source $MYVIMRC
			:source $MYGVIMRC
		endf
	endif

	map <silent>,v :tabnew $MYVIMRC<CR>
	if has('gui')
		map <silent>,u :call UpdateConfig()<CR>
	else
		map <silent>,u :source $MYVIMRC<CR>
	endif


	au! FileType svn,*commit*,*.txt setlocal spell spelllang=en,ru

	" переопределять переменные для некоторых файлов по расширению
	au! BufRead,BufNewFile,BufEnter *.json setf json
	au! BufRead,BufNewFile,BufEnter *.textile setf textile
	au! BufRead,BufNewFile,BufEnter TASKS setf TASKS
	au! BufRead,BufNewFile,BufEnter *.tpl setf templum

	" Auto reconfigure
	au! BufWritePost themerc,rc.xml,menu.xml :!openbox --reconfigure
	au! BufWritePost .Xdefaults              :!xrdb %
endif


" ФУНКЦИИ
if exists('*function')

	nmap <silent> \: :call AlignAssignBySymbol(":")<CR>
	nmap <silent> \# :call AlignAssignBySymbol("#")<CR>
	nmap <silent> \= :call AlignAssignBySymbol("=")<CR>
	nmap <silent> \- :call AlignAssignBySymbol("-")<CR>
	nmap <silent> \" :call AlignAssignBySymbol('"')<CR>
	nmap <silent> \\ :call AlignAssignBySymbol('/')<CR>
	nmap <silent> \| :call AlignAssignBySymbol('\|')<CR>
	nmap <silent> \{ :call AlignAssignBySymbol('{')<CR>
	fu! AlignAssignBySymbol(n)
		"Шаблоны, необходимые для нахождения операторов присваивания...
		let ASSIGN_OP   = '[-+*/%|&]\?'.a:n.'\@<!'.a:n.'['.a:n.'~]\@!'
		let ASSIGN_LINE = '^\(.\{-}\)\s*\(' . ASSIGN_OP . '\)'

		"Находим блок кода, с которым будем работать (непустые строки с одинаковым отступом)
		let indent_pat = '^' . matchstr(getline('.'), '^\s*') . '\S'
		let firstline  = search('^\%('. indent_pat . '\)\@!','bnW') + 1
		let lastline   = search('^\%('. indent_pat . '\)\@!', 'nW') - 1
		if lastline < 0
			let lastline = line('$')
		endif

		"Находим позицию в строке, по которой следует выравнивать операторы присваивания...
		let max_align_col = 0
		let max_op_width  = 0
		for linetext in getline(firstline, lastline)
			"В этой строке имеется оператор присваивания?
			let left_width = match(linetext, '\s*' . ASSIGN_OP)

			"Если оператор имеется, отслеживаем максимальные позицию в строке и
			"ширину оператора присваивания...
			if left_width >= 0
				let max_align_col = max([max_align_col, left_width])

				let op_width	  = strlen(matchstr(linetext, ASSIGN_OP))
				let max_op_width  = max([max_op_width, op_width+1])
			endif
		endfor

		"Код, необходимый для переформатирования строк таким образом,
		"чтобы выровнять операторы присваивания...
		let FORMATTER = '\=printf("%-*s%*s", max_align_col, submatch(1),
		\									max_op_width,  submatch(2))'

		" Переформатируем строки с операторами присваивания...
		for linenum in range(firstline, lastline)
			let oldline = getline(linenum)
			let newline = substitute(oldline, ASSIGN_LINE, FORMATTER, "")
			call setline(linenum, newline)
		endfor
	endf

	fu! MyTabLine()
		let s = ''
		for i in range(tabpagenr('$'))
			" select the highlighting
			if i + 1 == tabpagenr()
				let s .= '%#TabLineSel#'
			else
				let s .= '%#TabLine#'
			endif

			" set the tab page number (for mouse clicks)
			let s .= '%' . (i + 1) . 'T'

			" the label is made by MyTabLabel()
			let s .= ' %{MyTabLabel(' . (i + 1) . ')} '
		endfor

		" after the last tab fill with TabLineFill and reset tab page nr
		let s .= '%#TabLineFill#%T'

		return s
	endf
	fu! MyTabLabel(n)
		let buflist = tabpagebuflist(a:n)
		let winnr = tabpagewinnr(a:n)
		let label = fnamemodify(bufname(buflist[winnr - 1]), ':t')

		if label == ''
			if &buftype == 'quickfix'
				let label = '[Quickfix List]'
			else
				let label = 'NoName'
			endif
		endif

		if getbufvar(buflist[winnr - 1], "&modified")
			let label = "+".label
		endif

		let label = a:n.":".label

		return label
	endf
	set tabline=%!MyTabLine()

	" Remove trailing spaces
	"au! FileType python,c,json,cpp,hpp,perl call RemoveTrailingSpaces()
	map <silent> \rts :call RemoveTrailingSpaces()<cr>
	fu! RemoveTrailingSpaces()
		normal! mzHmy
		execute '%s:\s\+$::ge'
		normal! 'yzt`z
	endf

	" удалить DOS'овские ^M
	map <silent> \rf :call RepairFile()<cr>
	fu! RepairFile()
		normal! mzHmy
		execute '%s:::ge'
		normal! 'yzt`z
	endf

	" Jump to last cursor position when opening a file
	" dont do it when writing a commit log entry
	fu! SetCursorPosition()
		if &filetype !~ 'commit\c'
			if line("'\"") > 0 && line("'\"") <= line("$")
				exe "normal! g`\""
				normal! zz
			endif
		end
	endf
	au! BufReadPost * call SetCursorPosition()

	" Save backups every day
	fu! BackupDir()
		" Определим каталог для сохранения резервной копии
		if has('win32') || has('win64')
			let l:backupdir = $HOME.'/vimfiles/backup/'.substitute(substitute(expand('%:p:h'), '^'.$HOME, '~', ''), ':', '', '')
		else
			let l:backupdir = $HOME.'/.vim/backup/'.substitute(expand('%:p:h'), '^'.$HOME, '~', '')
		endif
		" если каталог не существует, создадим его рекурсивно
		if !isdirectory(l:backupdir)
			call mkdir(l:backupdir, 'p', 0700)
		endif
		" Переопределим каталог для резервных копий
		let &backupdir = l:backupdir
		" Переопределим расширение файла резервной копии
		let &backupext = strftime('~%Y-%m-%d~')
	endf
	au! BufWritePre * call BackupDir()

	nmap <silent> \ml :call AppendModeline()<CR>
	fu! AppendModeline()
		let l:modeline = printf(" vim: set fdm=%s fdc=%d ts=%d sw=%d tw=%d fo-=t ff=%s ft=%s:", &fdm, &fdc, &ts, &sw, &tw, &ff, &ft)
		" use substitute() instead of printf() to handle '%%s' modeline in LaTeX files.
		let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
		call append(line("$"), l:modeline)
	endf
endif


nmap <silent> \T :TaskList<CR>
imap <silent> \T <C-O>:TaskList<CR>
vmap <silent> \T <ESC>:TaskList<CR>

" USABILITY
vmap <silent> < <gv
vmap <silent> > >gv
nmap <silent> j gj
nmap <silent> k gk
nmap <silent> D dd

" toggle options
nmap <silent> \tw :setl wrap!<CR>
nmap <silent> \tn :setl number!<CR>

" Close
nmap <F10> :confirm q<CR>
imap <F10> <C-O>:confirm q<CR>
vmap <F10> <ESC>:confirm q<CR>
nmap <silent> <M-q> :confirm q<CR>
nmap <silent> <esc>q :confirm q<CR>

" Save
nmap <silent> <F2> :w<CR>
imap <silent> <F2> <C-O>:w<CR>
vmap <silent> <F2> <ESC>:w<CR>
nmap <silent> <M-s> :w<CR>
imap <silent> <M-s> <C-O>:w<CR>
vmap <silent> <M-s> <ESC>:w<CR>
nmap <silent> <esc>s :w<CR>
imap <silent> <esc>s <C-O>:w<CR>
vmap <silent> <esc>s <ESC>:w<CR>

" сохранять позицию при page up/down
nmap <PAGEUP> <C-U><C-U>
nmap <PAGEDOWN> <C-D><C-D>
imap <PAGEUP> <C-O><C-U><C-O><C-U>
imap <PAGEDOWN> <C-O><C-D><C-O><C-D>
vmap <PAGEUP> <C-U><C-U>
vmap <PAGEDOWN> <C-D><C-D>

" новая вкладка
nmap <silent> <C-T> :tabnew<CR>
imap <silent> <C-T> <C-O>:tabnew<CR>
vmap <silent> <C-T> <ESC>:tabnew<CR>

" Tabs navigation
nmap <silent> <M-j> :tabprev<CR>
imap <silent> <M-j> <C-O>:tabprev<CR>
vmap <silent> <M-j> <ESC>:tabprev<CR>
nmap <silent> <M-k> :tabnext<CR>
imap <silent> <M-k> <C-O>:tabnext<CR>
vmap <silent> <M-k> <ESC>:tabnext<CR>
" Fix for terminal
nmap <silent> <esc>k :tabnext<CR>
imap <silent> <esc>k <C-O>:tabnext<CR>
vmap <silent> <esc>k <ESC>:tabnext<CR>
nmap <silent> <esc>j :tabprev<CR>
imap <silent> <esc>j <C-O>:tabprev<CR>
vmap <silent> <esc>j <ESC>:tabprev<CR>

" Quick tab switch
for i in range(1,9)
	"let c = nr2char(i)
	exec "nmap <silent> <M-".i."> :tabnext ".i."<CR>"
	exec "imap <silent> <M-".i."> <C-O>:tabnext ".i."<CR>"
	exec "vmap <silent> <M-".i."> <ESC>:tabnext ".i."<CR>"
	exec "nmap <silent> <esc>".i." :tabnext ".i."<CR>"
	exec "imap <silent> <esc>".i." <C-O>:tabnext ".i."<CR>"
	exec "vmap <silent> <esc>".i." <ESC>:tabnext ".i."<CR>"
endfor

set langmap=йq,цw,уe,кr,еt,нy,гu,шi,щo,зp,х[,ъ],фa,ыs,вd,аf,пg,рh,оj,лk,дl,ж\\;,э',яz,чx,сc,мv,иb,тn,ьm,б\\,,ю.,ё`,ЙQ,ЦW,УE,КR,ЕT,НY,ГU,ШI,ЩO,ЗP,Х{,Ъ},ФA,ЫS,ВD,АF,ПG,РH,ОJ,ЛK,ДL,Ж:,Э\\",ЯZ,ЧX,СC,МV,ИB,ТN,ЬM,Б<,Ю>,Ё~

"if (filereadable(".vim"))
	"source ".vim"
"endif

"set secure
